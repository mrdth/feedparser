<?php
/**
 * Created by PhpStorm.
 * User: mrdth
 * Date: 29/09/18
 * Time: 22:35
 */


namespace NzbFeedParser;

class FeedParser
{
    protected $xml;

    public function __construct($xml = null)
    {
        $this->xml = new \SimpleXMLElement($xml);
    }

    public function loadXmlFile($file_path)
    {
        $this->xml = new \SimpleXMLElement(file_get_contents($file_path));
        $this->xml->registerXPathNamespace("atom", "http://www.w3.org/2005/Atom");
        $this->xml->registerXPathNamespace("newznab", "http://www.newznab.com/DTD/2010/feeds/attributes/");

        return $this;
    }

    public function getItems()
    {
        $items = [];
        foreach ($this->xml->channel->item as $item) {
            $items[] = (new NzbItem($item))->toArray();
        }

        return $items;
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: mrdth
 * Date: 29/09/18
 * Time: 23:46
 */

namespace NzbFeedParser;

class NzbItem
{
    protected $data;

    public function __construct(\SimpleXMLElement $data)
    {
        $this->data = $data;
    }

    public function toArray()
    {

        return [
            'title' => (string) $this->data->title,
            'link' => (string) $this->data->link,
            'guid' => $this->getNewznabAttr('guid'),
            'size' => (int) $this->getNewznabAttr('size'),
            'uploaded' => $this->getNewznabAttr('usenetdate'),
            'grabs' => $this->getNewznabAttr('grabs'),
            'movie' => [
                'imdb_id' => (int) $this->getNewznabAttr('imdb'),
                'title' => $this->getNewznabAttr('imdbtitle'),
                'tagline' => $this->getNewznabAttr('imdbtagline'),
                'plot' => $this->getNewznabAttr('imdbplot'),
                'score' => (float) $this->getNewznabAttr('imdbscore'),
                'year' => (int) $this->getNewznabAttr('imdbyear'),
                'runtime' => $this->getNewznabAttr('runtime'),
                'genre' => $this->getNewznabAttr('genre'),
                'director' => $this->getNewznabAttr('imdbdirector'),
                'actors' => $this->getNewznabAttr('imdbactors'),

            ]

        ];
    }

    public function getNewznabAttr($attribute)
    {
        $val = $this->data->xpath("descendant::newznab:attr[@name='$attribute']/@value");
        return count($val) ? (string) $val[0] : null;
    }
}
